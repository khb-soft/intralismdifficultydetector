﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntralismSharedEditor
{
    /// <summary>
    ///  Класс Helpers
    ///  содержит основные переменные
    /// </summary>
    public static class Helpers
    {
        public static List<EditorEventFunctionInfo> eventsMap = new List<EditorEventFunctionInfo>
        {
        new EditorEventFunctionInfo(
            "SpawnObj",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {
                new EditorEventFunctionInfo.EditorEventParametr("arc", EditorEventFunctionInfo.EditorEventParametrType.ArcSelector, ""),
                new EditorEventFunctionInfo.EditorEventParametr("hand", EditorEventFunctionInfo.EditorEventParametrType.Slider, "", "0,2,true,0")
            },
            "Spawn arcs for player. Try to make an interesting generation, synchronized with music"),

        new EditorEventFunctionInfo(
            "SetBGColor",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {
                new EditorEventFunctionInfo.EditorEventParametr("red", EditorEventFunctionInfo.EditorEventParametrType.Slider, "", "0,1,false"),
                new EditorEventFunctionInfo.EditorEventParametr("green", EditorEventFunctionInfo.EditorEventParametrType.Slider, "", "0,1,false"),
                new EditorEventFunctionInfo.EditorEventParametr("blue", EditorEventFunctionInfo.EditorEventParametrType.Slider, "", "0,1,false"),
                new EditorEventFunctionInfo.EditorEventParametr("speed", EditorEventFunctionInfo.EditorEventParametrType.InputField, "Lerp speed. Recomended 10", "float,10")
            },
            "Set camera background color"),

        new EditorEventFunctionInfo(
            "SetPlayerDistance",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {
                new EditorEventFunctionInfo.EditorEventParametr("distance", EditorEventFunctionInfo.EditorEventParametrType.Slider, "", "4,28,true,0")
            },
            "Set camera (or player) distance. Base player distance - 14"),

        new EditorEventFunctionInfo(
            "ShowTitle",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {
                new EditorEventFunctionInfo.EditorEventParametr("title", EditorEventFunctionInfo.EditorEventParametrType.InputField, ""),
                new EditorEventFunctionInfo.EditorEventParametr("duration", EditorEventFunctionInfo.EditorEventParametrType.InputField, "Message duration multiplier. Recomended 1", "float,1")
            },
            "Show text at the center of the screen. Usefull for quick messages like 'Thanks for playing!'"),

        new EditorEventFunctionInfo(
            "ShowSprite",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {
                new EditorEventFunctionInfo.EditorEventParametr("resource", EditorEventFunctionInfo.EditorEventParametrType.InputField, ""),
                new EditorEventFunctionInfo.EditorEventParametr("position", EditorEventFunctionInfo.EditorEventParametrType.Slider, "0 - background, 1 - foreground", "0,1,true,0"),
                new EditorEventFunctionInfo.EditorEventParametr("", EditorEventFunctionInfo.EditorEventParametrType.Toggle, "", "keep aspect ratio"),
                new EditorEventFunctionInfo.EditorEventParametr("duration", EditorEventFunctionInfo.EditorEventParametrType.InputField, "in seconds", "float,10")
            },
            "Show image from resources at the center of the screen at foreground or background"),

         new EditorEventFunctionInfo(
            "MapEnd",
            new List<EditorEventFunctionInfo.EditorEventParametr>()
            {

            },
            "Set the end of the map. Use this if you want to cut the long music.")
        };

        public static List<string> patternsMap = new List<string>
        {
            "[Up]", //0
            "[Right]", //1
            "[Down]", //2
            "[Left]", //3
            "[Up-Right-Left]", //4
            "[Up-Right-Down]", //5
            "[Right-Down-Left]", //6
            "[Up-Down-Left]", //7
            "[Up-Right-Down-Left]", //8
            "[PowerUp]", //9
            "[Up-Right]", //10
            "[Up-Down]", //11
            "[Up-Left]", //12
            "[Right-Down]", //13
            "[Right-Left]", //14
            "[Down-Left]", //15
        };

        public static List<string> tags = new List<string>
        {
            "Alternative",
            "Anime",
            "Blues",
            "Children",
            "Classical",
            "Dance",
            "Electronic",
            "Folk",
            "Hip-hop",
            "Indie",
            "Instrumental",
            "Jazz",
            "Metal",
            "Pop",
            "Rap",
            "Rock",
            "Soundtrack",
            "Other",
        };
    }
}
