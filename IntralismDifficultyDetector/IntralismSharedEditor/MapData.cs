﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IntralismSharedEditor
{
    /// <summary>
    /// Класс ивента
    /// </summary>
    public class MapEvent
    {
        public float time = 0;

        /// <summary>
        /// data[0] - ID метода
        /// data[1] - параметры метода
        /// </summary>
        public List<string> data = new List<string>();

        public MapEvent()
        {
        }

        public MapEvent(MapEvent mevent)
        {
            this.time = mevent.time;
            if (mevent.data != null)
            {
                this.data = new List<string>(mevent.data.Count);

                mevent.data.ForEach((item) =>
                {
                    data.Add(item);
                });
            }
        }

        public MapEvent(float time, List<string> data = null)
        {
            this.time = time;
            this.data = data;
        }
    }

    /// <summary>
    /// Класс ресурса карты
    /// </summary>
    public class MapResource
    {
        /// <summary>
        /// ID ресурса
        /// </summary>
        public string name = "";
        /// <summary>
        /// Тип ресурса, пока только Sprite
        /// </summary>
        public string type = "";
        /// <summary>
        /// Относительный путь
        /// </summary>
        public string path = "";
        /// <summary>
        /// MD5 сумма ресурса
        /// </summary>
        public string hash = "";

        public MapResource()
        {
        }

        public MapResource(string name, string type, string path, string hash)
        {
            this.name = name;
            this.type = type;
            this.path = path;
            this.hash = hash;
        }
    }

    /// <summary>
    /// Основной класс карты, содержит все данные
    /// </summary>
    public class MapData
    {
        /// <summary>
        /// ID должен соответствовать названию папки
        /// </summary>
        public string id;
        /// <summary>
        /// Название карты
        /// </summary>
        public string name = "No Name";
        /// <summary>
        /// Описание
        /// </summary>
        public string info = "No Info";
        /// <summary>
        /// Ресурсы - массив из объектов
        /// </summary>
        public List<MapResource> levelResources = new List<MapResource>() { };
        /// <summary>
        /// Массив тегов
        /// </summary>
        public List<string> tags = new List<string>() { };
        /// <summary>
        /// Количество рук
        /// </summary>
        public int handCount = 1;
        /// <summary>
        /// Ссылка, заданая пользователем
        /// </summary>
        public string moreInfoURL = "";
        /// <summary>
        /// Скорость
        /// </summary>
        public float speed = 15;
        /// <summary>
        /// Жизни
        /// </summary>
        public int lives = 5;
        /// <summary>
        /// Макс. жизней
        /// </summary>
        public int maxLives = 6;
        /// <summary>
        /// Путь до муз. файла
        /// </summary>
        public string musicFile = "music.ogg";
        /// <summary>
        /// Точная длительность музыки
        /// </summary>
        public float musicTime = 0;
        /// <summary>
        /// Путь до обложки
        /// </summary>
        public string iconFile = "icon.png";
        /// <summary>
        /// Условие доступности карты
        /// </summary>
        public List<string> unlockConditions = new List<string>();
        /// <summary>
        /// Скрывать, пока заблокирована
        /// </summary>
        public bool hidden = false;
        /// <summary>
        /// Массив чекпоинтов (их время в секундах)
        /// </summary>
        public List<float> checkpoints = new List<float>();
        /// <summary>
        /// Массив всех ивентов на карте
        /// </summary>
        public List<MapEvent> events = new List<MapEvent>();

        public MapData()
        { }

        public MapData(MapData mapData)
        {
            id = mapData.id;
            name = mapData.name;
            info = mapData.info;
            levelResources = mapData.levelResources;
            moreInfoURL = mapData.moreInfoURL;
            speed = mapData.speed;
            lives = mapData.lives;
            maxLives = mapData.maxLives;
            handCount = mapData.handCount;
            musicFile = mapData.musicFile;
            musicTime = mapData.musicTime;
            iconFile = mapData.iconFile;

            unlockConditions = new List<string>(mapData.unlockConditions.Count);
            mapData.unlockConditions.ForEach((item) =>
            {
                if (!string.IsNullOrEmpty(item))
                    unlockConditions.Add(item);
            });

            tags = new List<string>(mapData.tags.Count);
            mapData.tags.ForEach((item) =>
            {
                if (!string.IsNullOrEmpty(item))
                    tags.Add(item);
            });

            hidden = mapData.hidden;

            checkpoints = new List<float>(mapData.checkpoints.Count);
            mapData.checkpoints.ForEach((item) =>
            {
                checkpoints.Add(item);
            });

            events = new List<MapEvent>(mapData.events.Count);
            mapData.events.ForEach((item) =>
            {
                events.Add(new MapEvent(item));
            });
        }
    }

    public class EditorEventFunctionInfo
    {
        public enum EditorEventParametrType { InputField, Slider, Toggle, ArcSelector }
        public class EditorEventParametr
        {
            public string name;
            public EditorEventParametrType editor;
            public string description;
            public string data = null;

            /// <summary>
            /// Метод EditorEventParametr() является
            /// конструктором
            /// </summary>
            /// <param name="name">Имя параметра</param>
            /// <param name="editor">Тип редактора</param>
            /// <param name="description">Описание параметра</param>
            /// <param name="data">Дополнительные данные параметра</param>
            public EditorEventParametr(string name, EditorEventParametrType editor, string description, string data = null)
            {
                this.name = name;
                this.editor = editor;
                this.description = description;
                this.data = data;
            }
        }

        public List<EditorEventParametr> parameters;
        public string id;
        public string functionDescription;

        /// <summary>
        /// Метод EditorEventFunctionInfo() является
        /// конструктором
        /// </summary>
        /// <param name="id">Точное id ивента</param>
        /// <param name="parameters">Параметры ивента</param>
        /// <param name="functionDescription">Описание ивента</param>
        public EditorEventFunctionInfo(string id, List<EditorEventParametr> parameters, string functionDescription)
        {
            this.id = id;
            this.parameters = parameters;
            this.functionDescription = functionDescription;
        }
    }
}