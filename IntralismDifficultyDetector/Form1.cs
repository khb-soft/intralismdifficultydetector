﻿using IntralismSharedEditor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntralismDifficultyDetector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PrintHorizontalSplit();
            PrintLineBold(@"
  _____  _  __  __ _            _ _           _____       _            _             
 |  __ \(_)/ _|/ _(_)          | | |         |  __ \     | |          | |            
 | |  | |_| |_| |_ _  ___ _   _| | |_ _   _  | |  | | ___| |_ ___  ___| |_ ___  _ __ 
 | |  | | |  _|  _| |/ __| | | | | __| | | | | |  | |/ _ | __/ _ \/ __| __/ _ \| '__|
 | |__| | | | | | | | (__| |_| | | |_| |_| | | |__| |  __| ||  __| (__| || (_) | |   
 |_____/|_|_| |_| |_|\___|\__,_|_|\__|\__, | |_____/ \___|\__\___|\___|\__\___/|_|   
                                       __/ |                                         
  Code by Oxy949                      |___/                                          
  Algoritm by Nikl and Deko
            ");
            PrintHorizontalSplit();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SelectMapConfig();
        }

        private void SelectMapConfig()
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;
            MapData mapData = null;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Config files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    //filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    foreach (String file in openFileDialog.FileNames)
                    {
                        var fileStream = File.ReadAllText(file);
                        {
                            string workshopID = "";
                            if (new FileInfo(new FileInfo(file).Directory + "/workshop.txt").Exists)
                            {
                                workshopID = File.ReadAllText(new FileInfo(new FileInfo(file).Directory + "/workshop.txt").FullName);
                            }
                            fileContent = fileStream;
                            try
                            {
                                textBox1.Text = filePath;
                                mapData = CustomEditor.GetMap(fileContent);
                            }
                            catch (Exception e)
                            {
                                mapData = null;
                                MessageBox.Show("Bad config", "Error openning " + filePath, MessageBoxButtons.OK);
                            }

                            if (mapData != null)
                                CalcMapDifficulty(mapData, workshopID);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Считает сложность карты
        /// </summary>
        /// <param name="mapData">карта</param>
        public void CalcMapDifficulty(MapData mapData, string workshopID = "")
        {
            var newMapData = new MapData(mapData);

            //Очищаем текст
            //Clear();

            //Пишем крутое лого


            //Пишем имя карты из конфига
            PrintFormated("Map", newMapData.name);
            if (!string.IsNullOrEmpty(workshopID))
                PrintFormated("Workshop ID", workshopID);

            // Обновляем заголовок окна
            //this.Text = "Intralism Difficulty Detector | " + newMapData.name;

            //Берем скорость карты из конфига и запоминаем
            double speed = mapData.speed;
            //Стандартный зум
            double zoom = 14;


            //Дальше находим время первой и последней арок
            double firstArcTime = 0;
            double lastArcTime = newMapData.musicTime;

            bool wasFirst = false;
            foreach (var ev in newMapData.events) //Идём по всем евентам
            {
                if (ev.data[0] == "SpawnObj") //Если евент SpawnObj
                {
                    if (!wasFirst)
                    {
                        firstArcTime = ev.time;
                        wasFirst = true;
                    }
                    lastArcTime = ev.time;
                }
            }

            //Нашли, показываем
            //PrintFormated("First arc", firstArcTime);
            //PrintFormated("Last arc", lastArcTime);

            //Длинна песни по первой и последней арке
            double songLength = lastArcTime - firstArcTime;
            //PrintFormated("Song Length", songLength);

            //Текущие зум и скорость. Будем менять при прохождении цикла по секундам. Считаем начальные kzoom и kspeed
            double kzoom = CalcKZoom(zoom);
            double kspeed = CalcKSpeed(speed);

            // Теперь пройдёмся по всем секундам в песне. В этих переменных будем хранить нужные значения для подсчёта

            // Для подсчёта средней сложности
            double summDiff = 0;
            int diffCount = 0;

            // Для поиска макс. сложности
            double maxDiff = 0;

            //Будет хранит секунды, когда есть арки
            int realLength = 0;
            List<double> maxList = new List<double>();
            for (int second = 0; second < (int)Math.Ceiling(lastArcTime); second++) // Проходимся циклом с 0 до значения lastArcTime (в большую сторону)
            {
                //PrintLine(Second + ":");

                int pressesPerCurrentSecond = 0;
                double diffPerCurrentSecond = 0;

                foreach (var ev in newMapData.events) // Ищем евенты, которые происходят в данную секунды
                {
                    if (ev.time >= second && ev.time < second + 1)
                    {
                        if (ev.data[0] == "SpawnObj")
                        {
                            int pressesCount = ev.data[1].Count(f => f == '-') + 1; // получаем кол-во арок в евенте
                            pressesPerCurrentSecond += pressesCount;

                            // Коэф. сложности для арок
                            double karc = CalcKArc(pressesCount);

                            //PrintFormated("kps: ", pressesPerCurrentSecond);
                            //PrintLine("Текущие A:" + karc + " Z:" + kzoom + " S:" + kspeed);
                            //PrintLine("EventDifficulty:" + (karc * kzoom * kspeed));

                            diffPerCurrentSecond += (karc * kzoom * kspeed);
                        }

                        if (ev.data[0] == "SetPlayerDistance")
                        {
                            float zoomValue = float.Parse(ev.data[1], System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo); // Берем значение зума (оно текстовое, поэтому конвертим в число)

                            kzoom = CalcKZoom(zoomValue);

                            //PrintLine("Zoom:" + ev.time + " = " + zoomValue + " kzoom:" + kzoom);
                        }

                        if (ev.data[0] == "SetSpeed")
                        {
                            float speedValue = float.Parse(ev.data[1], System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo); // Берем значение скорости (оно текстовое, поэтому конвертим в число)

                            kspeed = CalcKSpeed(speedValue);

                            //PrintLine("Speed:" + ev.time + " = " + speedValue + " kspeed:" + kspeed);
                        }
                    }
                }

                //PrintLine("Presses/Second: " + pressesPerCurrentSecond);
                //PrintLine("Difficulty/Second: " + diffPerCurrentSecond);

                // Для этой секунды считаем:
                if (pressesPerCurrentSecond > 0) // Если есть нажатия
                    realLength += 1;
                if (maxDiff < diffPerCurrentSecond) // Если сложность эой секунды меньше обнаруженной ранее
                    maxDiff = diffPerCurrentSecond;

                // Для подсчёта средней сложности, считаем сумму и кол-во эл-ов
                if (diffPerCurrentSecond > 0)
                {
                    summDiff += diffPerCurrentSecond;
                    diffCount++;
                    maxList.Add(diffPerCurrentSecond);
                }
            }
            int realLength33 = (int)Math.Ceiling(realLength * 0.33);
            //PrintFormated("33%Length", realLength33);
            //PrintFormated("reallength11111111111", realLength);
            double summMax33 = 0;
            for (int second = 0; second < realLength33; second++)
            {
                var max = maxList.OrderBy(d => -d).ToList()[second];
                summMax33 += max;
                //PrintLine("33%hp" + max);
            }
            double Avg33 = summMax33 / (double)realLength33;
            //PrintFormated("avg33", Avg33);

            // Цикл по секундам закончен. Теперь обрабатываем данные
            //double avgDiff = summDiff / (double)diffCount;
            //PrintFormated("AVG Difficulty", avgDiff);
            //PrintFormated("Max Difficulty", maxDiff);
            //double mapDiff = (avgDiff + maxDiff) / 2.0f;
            //PrintFormated("Map Difficulty", mapDiff);

            double kLength = Math.Log(realLength, 60);
            //PrintFormated("K Length", kLength);

            double finalDifficulty = kLength * Avg33;
            PrintFormatedBold("Result Difficulty", finalDifficulty);
            PrintFormatedBold("Max Score", GetMapMaxScore(mapData));
            
            PrintLine("");
        }

        double CalcKArc(int pressesCount)
        {
            return -((double)(pressesCount * pressesCount) / 4.0f) + 1.3f * pressesCount;
        }

        double CalcKSpeed(double speed)
        {
            return (speed / 14.0f);
        }

        double CalcKZoom(double zoom)
        {
            if (zoom < 14)
            {
                return 14.0f / zoom;
            }
            else
                return ((double)(zoom * zoom) / (14.0f * 14.0f)) - zoom / 7.0f + 2;
        }

        public int GetMapMaxScore(MapData mapData)
        {
            var events = new MapData(mapData).events.Where(x => x.data[0] == "SpawnObj").ToList();
            int score = 0;
            int combo = 0;

            foreach (var eEvent in events)
            {
                int arcs = eEvent.data[1].Split('-').Length;
                for (int arcNumber = 0; arcNumber < arcs; arcNumber++)
                {
                    combo += 1;
                    score += combo;
                }
            }

            return score;
        }

        #region IO
        void Clear()
        {
            richTextBox1.Clear();
        }

        void Print(string text)
        {
            richTextBox1.AppendText(text);
        }

        void PrintLine(string text)
        {
            richTextBox1.AppendText(text + Environment.NewLine);
        }

        void PrintFormated(string name, object variable)
        {
            string v = "";
            if (variable.GetType() == typeof(double) || variable.GetType() == typeof(float))
                v = String.Format("{0:0.00}", variable);
            else
                v = variable.ToString();

            PrintLine(String.Format("{0,-42} {1,42}", name, v));
        }

        void PrintFormatedBold(string name, object variable)
        {

            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            PrintFormated(name, variable);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
        }

        void PrintHorizontalSplit()
        {
            PrintLine("--------------------------------------------------------------------------------------");
        }

        void PrintBold(string text)
        {
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            Print(text);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
        }

        void PrintLineBold(string text)
        {
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            PrintLine(text);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}